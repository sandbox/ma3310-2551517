<?php

/**
 * @file
 * Administration functions for WeChat Lite module.
 */

/**
 * Determines if segment exists as a service account.
 *
 * @param string $segment
 *   The ID of the text format to check.
 *
 * @return bool
 *   TRUE if segment exists as a service account, FALSE otherwise.
 *
 */
function _wechat_lite_service_accounts_segment_exists($segment) {

  $service_accounts = variable_get(WECHAT_LITE__VARIABLE__SERVICE_ACCOUNTS, array());

  $ret = FALSE;
  if (isset($service_accounts[$segment])) {
    $ret = TRUE;
  }

  return $ret;
}

/**
 * Determines if segment exists as a enterprise account.
 *
 * @param $segment string
 *   The ID of the text format to check.
 *
 * @return bool
 *   TRUE if segment exists as a service account, FALSE otherwise.
 *
 */
function enterprise_accounts_segment_exists($segment) {

  $accounts = variable_get(WECHAT_LITE__VARIABLE__ENTERPRISE_ACCOUNTS, array());

  $ret = FALSE;
  if (isset($accounts[$segment])) {
    $ret = TRUE;
  }

  return $ret;
}

/**
 * 
 */
function wechat_lite_admin_create_menu_service_account($segment){
  $service_accounts = variable_get(
    WECHAT_LITE__VARIABLE__SERVICE_ACCOUNTS,
    array()
  );

  if (empty($service_accounts[$segment])) {

    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      "Service Account: account %segment isn't configured",
      array('%segment' => $segment)
    );

    // TODO: goto creation fail page
    return FALSE;
  }

  $service_account = $service_accounts[$segment];

  $get_token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$service_account['appid']."&secret=".$service_account['appsecret'];

  $res = drupal_http_request($get_token_url);

  if (isset($res->data)) {

    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      "Service Account Create Menu: token data = %data",
      array('%data' => $res->data),
      WATCHDOG_INFO
    );

    $json = json_decode($res->data, true);

    if (!$json || !empty($json['errcode'])) {
      // TODO: goto creation fail page
      return NULL;
    }

    $json['access_token'];

    $get_menu_url = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=".$json['access_token'];

    $res = drupal_http_request($get_menu_url);

    if ($res->data) {

      watchdog(
        WECHAT_LITE__HUMAN_NAME,
        "Service Account Create Menu: menu data = %data",
        array('%data' => $res->data),
        WATCHDOG_INFO
      );

      $json = json_decode($res->data, true);

      if (!$json || !empty($json['errcode'])) {
        // TODO: goto creation fail page
        return NULL;
      }

      var_dump($json);
    
    }
  }
}

function wechat_lite_menu_form($form, &$form_state, $account_segment = NULL) {

  $form['menu_0'] = array(
    '#type' => 'fieldset',
    '#title' => t('First Menu'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['menu_0']['item_0'] = array(
    '#type' => 'fieldset',
    '#title' => t('First Item'),
  );

  $form['menu_0']['item_0']['item_name_0_0'] = array(
    '#type' => 'textfield',
    '#title' => t('Item Name'),
  );

  $form['menu_0']['item_0']['item_value_0_0'] = array(
    '#type' => 'textfield',
    '#title' => t('Item Value'),
    '#description' => t('Please specify URL, Oauth Targets or Event ID '),
  );

  $form['menu_0']['item_0']['item_type_0_0'] = array(
    '#type' => 'radios',
    '#title' => t('Item Type'),
    '#options' => array(
      'URL'=>t('URL'),
      'OauthS'=>t('Oauth Silence'),
      'OauthE'=>t('Oauth Explicit'),
      'Event'=>t('Event'),
    ),
    '#attributes' => array(
      'class' => array('container-inline'),
    ),


  );

  $form['menu_0']['item_1'] = array(
    '#type' => 'fieldset',
    '#title' => t('Second Item'),
  );

  $form['menu_0']['item_2'] = array(
    '#type' => 'fieldset',
    '#title' => t('Third Item'),
  );

  $form['menu_0']['item_3'] = array(
    '#type' => 'fieldset',
    '#title' => t('Forth Item'),
  );

  $form['menu_0']['item_4'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fifth Item'),
  );

  $form['second_menu'] = array(
    '#type' => 'fieldset',
    '#title' => t('Second Menu'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['third_menu'] = array(
    '#type' => 'fieldset',
    '#title' => t('Third Menu'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;

}

/**
 * Form constructor for the WeChat Service Account add/edit form.
 *
 * @param $account
 *   A format object having the properties:
 *   - format: A machine-readable name representing the ID of the text format to
 *     save. If this corresponds to an existing text format, that format will be
 *     updated; otherwise, a new format will be created.
 *   - name: The title of the text format.
 *   - cache: An integer indicating whether the text format is cacheable (1) or
 *     not (0). Defaults to 1.
 *   - status: (optional) An integer indicating whether the text format is
 *     enabled (1) or not (0). Defaults to 1.
 *   - weight: (optional) The weight of the text format, which controls its
 *     placement in text format lists. If omitted, the weight is set to 0.
 *
 * @see wechat_lite_service_account_form_validate()
 * @see wechat_lite_service_account_form_submit()
 * @ingroup forms
 */
function wechat_lite_service_account_form($form, &$form_state, $account_segment = NULL) {

  // if $account is empty, need initial $account, treat this situation as Account Adding
  if (!isset($account_segment)) {
    drupal_set_title(t('Add WeChat Service Account'));
    $account = (object) array(
      'name' => '',
      'segment' => '',
      'appid' => '',
      'appsecret' => '',
      'token' => '',
    );
  }
  else {
    $service_accounts = variable_get(WECHAT_LITE__VARIABLE__SERVICE_ACCOUNTS, array());
    $account = (object)$service_accounts[$account_segment];
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Account Name'),
    '#description' => t('Give a name to identify each WeChat Service Account.'),
    '#size' => 20,
    '#default_value' => $account->name,
    '#required' => TRUE,
  );

  $form['segment'] = array(
    '#type' => 'machine_name',
    '#required' => TRUE,
    '#default_value' => $account->segment,
    '#maxlength' => 255,
    '#machine_name' => array(
      'exists' => 'service_accounts_segment_exists',
    ),
    '#disabled' => !empty($account->segment),
  );

  $form['appid'] = array(
    '#type' => 'textfield',
    '#title' => t('appID'),
    '#size' => 20,
    '#default_value' => $account->appid,
    '#required' => TRUE,
  );

  $form['appsecret'] = array(
    '#type' => 'textfield',
    '#title' => t('appsecret'),
    '#description' => t(<<<'TAG'
Get this information from privilege management sub-page of settings page.<br>
Application has to be put in non-SystemAdmin group first, then use Secret of that group.
TAG
    ),
    '#default_value' => $account->appsecret,
    '#required' => TRUE,
  );

  $form['token'] = array(
    '#type' => 'textfield',
    '#title' => t('Token'),
    '#default_value' => $account->token,
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Account'),
  );

  return $form;
}


function wechat_lite_service_account_form_submit($form, &$form_state) {

  $account = array(
    'name' => $form['name']['#value'],
    'segment' => $form['segment']['#value'],
    'appid' => $form['appid']['#value'],
    'appsecret' => $form['appsecret']['#value'],
    'token' => $form['token']['#value'],
    'weight' => 0,
  );

  $accounts = variable_get(WECHAT_LITE__VARIABLE__SERVICE_ACCOUNTS, array());
  $accounts[$account['segment']] = $account;
  variable_set(WECHAT_LITE__VARIABLE__SERVICE_ACCOUNTS, $accounts);

  $form_state['redirect'] = 'admin/config/system/wechat_lite/service_accounts';
}


function wechat_lite_enterprise_account_form($form, &$form_state, $account_segment = NULL) {

  // if $account is empty, need initial $account, treat this situation as Account Adding
  if (!isset($account_segment)) {
    drupal_set_title(t('Add WeChat Enterprise Account'));
    $account = (object) array(
      'name' => '',
      'segment' => '',
      'corpid' => '',
      'appid' => '',
      'token' => '',
      'appsecret' => '',
      'encodingAesKey' => '',
    );
  }
  else {
    $accounts = variable_get(WECHAT_LITE__VARIABLE__ENTERPRISE_ACCOUNTS, array());
    $account = (object)$accounts[$account_segment];
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Account Name'),
    '#description' => t('Give a name to identify each WeChat Enterprise Account.'),
    '#size' => 20,
    '#default_value' => $account->name,
    '#required' => TRUE,
  );

  $form['segment'] = array(
    '#type' => 'machine_name',
    '#required' => TRUE,
    '#default_value' => $account->segment,
    '#maxlength' => 255,
    '#machine_name' => array(
      'exists' => 'enterprise_accounts_segment_exists',
    ),
    '#disabled' => !empty($account->segment),
  );

  $form['corpid'] = array(
    '#type' => 'textfield',
    '#title' => t('CorpID'),
    '#description' => t(<<<'TAG'
Get this information from company setting page.
TAG
    ),
    '#size' => 20,
    '#default_value' => $account->corpid,
    '#required' => TRUE,
  );

  $form['appid'] = array(
    '#type' => 'textfield',
    '#title' => t('AppID'),
    '#description' => t(<<<'TAG'
Get this information from application homepage.
TAG
    ),
    '#size' => 20,
    '#default_value' => $account->appid,
    '#required' => TRUE,
  );

  $form['token'] = array(
    '#type' => 'textfield',
    '#title' => t('Token'),
    '#default_value' => $account->token,
    '#required' => TRUE,
  );

  $form['appsecret'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret'),
    '#description' => t(<<<'TAG'
Get this information from privilege management sub-page of settings page.<br>
Application has to be put in non-SystemAdmin group first, then use Secret of that group.
TAG
    ),
    '#default_value' => $account->appsecret,
    '#required' => TRUE,
  );

  $form['encodingAesKey'] = array(
    '#type' => 'textfield',
    '#title' => t('encodingAesKey'),
    '#default_value' => $account->encodingAesKey,
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Account'),
  );

  return $form;

}


function wechat_lite_enterprise_account_form_submit($form, &$form_state) {

  $account = array(
    'name' => $form['name']['#value'],
    'segment' => $form['segment']['#value'],
    'corpid' => $form['corpid']['#value'],
    'appid' => $form['appid']['#value'],
    'token' => $form['token']['#value'],
    'encodingAesKey' => $form['encodingAesKey']['#value'],
    'weight' => 0,
  );

  $accounts = variable_get(WECHAT_LITE__VARIABLE__ENTERPRISE_ACCOUNTS, array());
  $accounts[$account['segment']] = $account;
  variable_set(WECHAT_LITE__VARIABLE__ENTERPRISE_ACCOUNTS, $accounts);

  $form_state['redirect'] = 'admin/config/system/wechat_lite/enterprise_accounts';

}

/**
 * Form builder.
 * Configure Service Accounts related options.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function wechat_lite_service_accounts_settings($form, &$form_state) {

  $form['#tree'] = TRUE;

  $service_accounts = variable_get(WECHAT_LITE__VARIABLE__SERVICE_ACCOUNTS, array());

  $form['service_accounts']['#theme'] = "uitweak_admin_form_2_draggable_table";

  // Put array as raw table, need theme to render
  foreach ($service_accounts as $segment => $account) {
    $form['service_accounts'][$segment]['Account Name'] = array('#markup' => drupal_placeholder($account['name']));
    $form['service_accounts'][$segment]['WeChat Callback URL'] = array(
      '#theme' => 'item_list',
      '#items' => array(
        t('<strong>Callback URL</strong><br>') . url('wechat_lite/service_account/server/' . $segment, array('absolute'=>TRUE)),
        t('<strong>Register URL Template</strong><br>') . format_string(WECHAT_LITE__URL__OAUTH_BASIC, array(
          '@app_id' => $account['appid'],
          '@oauth_url' => url('wechat_lite/service_account/oauth/' . $segment, array('absolute' => TRUE)),
          '@state' => 'summit_register__@summit_code',
        )),
        t('<strong>Check-in URL Template</strong><br>') . format_string(WECHAT_LITE__URL__OAUTH_BASIC, array(
          '@app_id' => $account['appid'],
          '@oauth_url' => url('wechat_lite/service_account/oauth/' . $segment, array('absolute' => TRUE)),
          '@state' => 'summit_sign-in__@summit_code',
        )),
      ),
      '#attributes' => array('style' => array('word-break: break-all;')),
    );

    $form['service_accounts'][$segment]['weight'] = array(
      '#type' => 'textfield',
      '#size' => 5,
      '#default_value' => 0,
      '#attributes' => array('class' => array('row-weight')),
    );

    $hidden_data = array();
    $hidden_data['name'] = array(
      '#type' => 'hidden',
      '#value' => $account['name'],
    );
    $hidden_data['segment'] = array(
      '#type' => 'hidden',
      '#value' => $account['segment'],
    );
    $hidden_data['appid'] = array(
      '#type' => 'hidden',
      '#value' => $account['appid'],
    );
    $hidden_data['appsecret'] = array(
      '#type' => 'hidden',
      '#value' => $account['appsecret'],
    );
    $hidden_data['token'] = array(
      '#type' => 'hidden',
      '#value' => $account['token'],
    );

    $form['service_accounts'][$segment]['Operations'][] = $hidden_data;

    $operations = array(
      array(
        'title' => t('Configure'),
        'href' => 'admin/config/system/wechat_lite/service_account/' . $segment,
        'query' => array('destination' => 'admin/config/system/wechat_lite/service_accounts'),
      ),
      array(
        'title' => t('Get menu'),
        'href' => 'admin/config/system/wechat_lite/create_menu/service_account/' . $segment,
        'query' => array('destination' => 'admin/config/system/wechat_lite/service_accounts'),
      ),
    );

    $form['service_accounts'][$segment]['Operations'][] = array(
      '#theme' => 'links__ctools_dropbutton',
      '#links' => $operations,
    );

  }

  $form['#submit'][] = 'wechat_lite_service_accounts_settings_submit';

  return system_settings_form($form);

}

/**
 * Form builder.
 * Configure Enterprise Accounts.
 * 1. List all configured enterprise accounts
 *
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function wechat_lite_enterprise_accounts_settings($form, &$form_state) {

  $form['#tree'] = TRUE;



  return system_settings_form($form);

}

/**
 * Form Validation
 * prevent invalid values are inputted.
 *
 *
 *
 * @param $form
 * @param $form_state
 */
function wechat_lite_service_accounts_settings_validate($form, &$form_state) {
  /*if($form['name']['#value'] == '')
      form_set_error('name', 'Account name can not be empty.');
  elseif($form['appid']['value'] == '')
      form_set_error('appid', 'Appid can not be empty.');
  elseif($form['appsecret']['value'] == '')
      form_set_error('appid', 'Appsecret can not be empty.');
  elseif($form['token']['value'] == '')
      form_set_error('token', 'Token can not be empty.');*/
}

/**
 *
 *
 * @param $form
 * @param $form_state
 *
 */
function wechat_lite_service_accounts_settings_submit($form, &$form_state) {
  $data = $form_state['values']['service_accounts'];
  $data = uitweak_draggable_table_data_trim_weight($data);

  $accounts = array();
  foreach ($data as $segment => $account){
    $account = $account['Operations'][0];
    $accounts[$account['segment']] = $account;
  }

  variable_set(WECHAT_LITE__VARIABLE__SERVICE_ACCOUNTS, $accounts);
}