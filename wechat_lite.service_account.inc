<?php

require_once realpath(dirname(__FILE__)) . '/include/WXBizMsgCrypt.php';

/**
 *
 * @param $segment string
 * @return NULL | mixed
 */
function wechat_lite_service_account_callback ($segment) {

  watchdog(
    WECHAT_LITE__HUMAN_NAME,
    __FUNCTION__ . ": invoked.",
    array(),
    WATCHDOG_NOTICE
  );

  $service_account = wechat_lite_get_service_account($segment);

  if (empty($service_account)){
    header(t(WECHAT_LITE__ERROR_MSG__NO_SERVICE), TRUE, 503);
    return MENU_SITE_ONLINE;
  }

  // Get $_GET contents to decide next logic
  $query = drupal_get_query_parameters();

  if(!wechat_lite_check_request_signature($service_account['token'], $query)){
    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      __FUNCTION__ . ": Check Signature failed!",
      array(),
      WATCHDOG_ERROR
    );

    header(t(WECHAT_LITE__ERROR_MSG__NO_SERVICE), TRUE, 503);
    return MENU_SITE_ONLINE;
  }

  // <-- Verify Echo String
  if (isset($query['echostr'])) {

    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      __FUNCTION__ . " echostr: %echostr",
      array('%echostr' => $query['echostr'])
    );

    echo $query['echostr'];

    return MENU_SITE_ONLINE;
  }

  $postData = file_get_contents("php://input");

  if (empty($postData)){
    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      "Service Account: No Post Data",
      array(),
      WATCHDOG_WARNING
    );

    return MENU_SITE_ONLINE;
  }
  else {

    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      "Service Account: Post Data = %postData",
      array('%postData' => $postData)
    );

  }

  // Message handler start


}

function wechat_lite_check_request_signature($token, &$query) {

  if (empty($query["signature"]) || empty($query["timestamp"]) || empty($query["nonce"])){
    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      "Service Account: Check Signature failed - parameter empty."
    );
    return FALSE;
  }

  $signature = $query["signature"];
  $timestamp = $query["timestamp"];
  $nonce = $query["nonce"];

  $tmpArr = array($token, $timestamp, $nonce);
  // use SORT_STRING rule
  sort($tmpArr, SORT_STRING);
  $tmpStr = implode( $tmpArr );
  $tmpStr = sha1( $tmpStr );

  if( $tmpStr == $signature ){
    return TRUE;
  }
  else{
    return FALSE;
  }
}

//function wechat_lite_callback(){

//  $wechat_lite_obj = _wechat_lite_init_obj();
//  $wechat_lite_obj->valid();
//
//  $type = $wechat_lite_obj->getRev()->getRevType();
//  switch($type){
//    case Wechat::MSGTYPE_TEXT: {
//      $content = $wechat_lite_obj->getRev()->getRevContent();
//      if($content == 'reg'){
//        $oauth2 = $wechat_lite_obj->getOauthRedirect('http://xiaoguigh.tunnel.mobi/drupal/?q=wechat_lite_oauth2','1','snsapi_base');
//        $text = '<a href="'.$oauth2.'">点击登录</a>';
//        $wechat_lite_obj->text($text)->reply();
//      }
//      else
//        $wechat_lite_obj->text('your msgtype is text')->reply();
//    }
//    case Wechat::MSGTYPE_EVENT: {
//      $array = $wechat_lite_obj->getRev()->getRevEvent();
//      switch($array['event']){
//        case Wechat::EVENT_SUBSCRIBE:
//          $wechat_lite_obj->text('welcome')->reply();
//          break;
//        case Wechat::EVENT_UNSUBSCRIBE:
//          break;
//      }
//      break;
//    }
//  }

//  exit;
//}


function wechat_lite_service_account_oauth($segment = NULL){

  watchdog(
    WECHAT_LITE__HUMAN_NAME,
    __FUNCTION__ . ": invoked.",
    array(),
    WATCHDOG_NOTICE
  );

  $service_account = wechat_lite_get_service_account($segment);

  if (empty($service_account)){
    header(t(WECHAT_LITE__ERROR_MSG__NO_SERVICE), TRUE, 503);
    return MENU_SITE_ONLINE;
  }

  $state = isset($_GET['state'])?$_GET['state']:'';

  if (empty($state)){

    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      __FUNCTION__ . ": no state",
      array(),
      WATCHDOG_ERROR
    );

    header(t(WECHAT_LITE__ERROR_MSG__NO_SERVICE), TRUE, 503);
    return NULL;
  }
  else {

    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      "Service Account oauth: state = %state",
      array('%state' => $state),
      WATCHDOG_INFO
    );
  }

  $code = isset($_GET['code'])?$_GET['code']:'';

  if (!$code) {

    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      "Service Account oauth: no code is passed",
      array(),
      WATCHDOG_ERROR
    );

    header(t(WECHAT_LITE__ERROR_MSG__NO_SERVICE), TRUE, 503);
    return MENU_SITE_ONLINE;
  }

  $code2token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$service_account['appid'].'&secret='.$service_account['appsecret'].'&code='.$code.'&grant_type=authorization_code';

  $res = drupal_http_request($code2token_url);

  if (isset($res->data)) {

    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      "Service Account oauth: token data = %data",
      array('%data' => $res->data),
      WATCHDOG_INFO
    );

    $json = json_decode($res->data, true);

    if (!$json || !empty($json['errcode'])) {
      header(t(WECHAT_LITE__ERROR_MSG__NO_SERVICE), TRUE, 503);
      return NULL;
    }

    $json['access_token'];
    $json['refresh_token'];
    $json['openid'];
    $_SESSION['openid'] = $json['openid'];

    //TODO invoke all registered hook
    $map = module_invoke_all('wechat_lite_oauth_state_to_uri', $json['openid']);

    if (empty($map[$state])){
      watchdog(
        WECHAT_LITE__HUMAN_NAME,
        __FUNCTION__ . ": no action configured for state",
        array(),
        WATCHDOG_ERROR
      );

      header(t(WECHAT_LITE__ERROR_MSG__NO_SERVICE), TRUE, 503);
      return MENU_NOT_FOUND;
    }

    drupal_goto($map[$state]);
    return MENU_SITE_ONLINE;
  }
  else {

    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      "Service Account oauth: no token data, URL = %url",
      array('%url' => $code2token_url),
      WATCHDOG_ERROR
    );
    header(t(WECHAT_LITE__ERROR_MSG__NO_SERVICE), TRUE, 503);
    return NULL;
  }


}


//function oauth2_callback($service_account_name){
//
//  if (empty($service_account_name)) {
//    header(t(WECHAT_LITE__ERROR_MSG__NO_SERVICE), TRUE, 503);
//    return;
//  }
//
//  $wechat_lite_obj = _wechat_lite_init_obj();
//
//  $uri = $_GET['state'];
//  $userinfo = $wechat_lite_obj->getOauthAccessToken();
//
//  $pwd = '12345678';
//  $name = $userinfo['openid'];
//  $options = array(
//    'name' => $name,
//    'mail' => $name.'@geconf.com',
//    'pass' => $pwd,
//    'status' => 1,
//    'init' => 'email address',
//    'roles' => array(
//      DRUPAL_AUTHENTICATED_RID => 'cyrus_authenticated user',
//      4 => 'user',
//    ),
//  );
//  if(!user_load_by_mail($options['mail'])){
//    $account = user_save('', $options);
//  }
//
//  if($uid = user_authenticate($name, $pwd)){
//    $user = user_load($uid);
//    $form_state = array();
//    $form_state['uid'] = $uid;
//    user_login_submit(array(),$form_state);
//    drupal_session_regenerate();
//    drupal_goto('/'.$uri);
//  }
//
//
//}

/**
 * Returns service account details per segment provided.
 *
 * @param string $segment
 * @return array
 */
function wechat_lite_get_service_account($segment) {

  if (empty($segment)) {

    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      __FUNCTION__ . ": account segment isn't provided.",
      array(),
      WATCHDOG_ERROR
    );

    return array();
  }

  watchdog(
    WECHAT_LITE__HUMAN_NAME,
    __FUNCTION__ . " segment: %segment",
    array('%segment' => $segment),
    WATCHDOG_INFO
  );

  // Get all configured accounts information
  $service_accounts = variable_get(
    WECHAT_LITE__VARIABLE__SERVICE_ACCOUNTS,
    array()
  );

  if (empty($service_accounts[$segment])) {

    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      "Service Account: account %segment isn't configured",
      array('%segment' => $segment)
    );

    return array();
  }

  return $service_accounts[$segment];
}