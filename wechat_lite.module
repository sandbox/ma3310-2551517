<?php
/**
 * @file
 *
 * Module entry file.
 *
 */

/**
 *
 */
define('WECHAT_LITE__HUMAN_NAME', 'WeChat Lite');

define('WECHAT_LITE__ERROR_MSG__NO_SERVICE', t('Service is not available!'));

/**
 * Module variable names.
 */
define('WECHAT_LITE__VARIABLE__SERVICE_ACCOUNTS', 'wechat_lite_service_accounts');
define('WECHAT_LITE__VARIABLE__ENTERPRISE_ACCOUNTS', 'wechat_lite_enterprise_accounts');

define('WECHAT_LITE__URL__OAUTH_BASIC', 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=@app_id&redirect_uri=@oauth_url&response_type=code&scope=snsapi_base&state=@state#wechat_redirect');

define('WECHAT_LITE__URL__ACCESS_TOKEN_APP', 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=@APPID&secret=@APPSECRET');
define('WECHAT_LITE__URL__ACCESS_TOKEN_USER', 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=@APPID&secret=@APPSECRET');

/**
 * Implements hook_menu().
 */
function wechat_lite_menu() {

  $items['wechat_lite'] = array(
    'title' => 'wechat',
    'description' => 'wechat_lite callback',
    'page callback' => 'wechat_lite_callback',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'wechat_lite.service_account.inc',
  );

  $items['wechat_lite/oauth2'] = array(
    'title' => 'WeChat',
    'description' => 'oauth2 callback',
    'page callback' => 'oauth2_callback',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'wechat_lite.service_account.inc',
  );

  // just create admin block
  $items['admin/config/system/wechat_lite'] = array(
    'title' => 'WeChat Lite',
    'description' => t('Allow site to connect to WeChat accounts.'),
    'page callback' => 'system_admin_menu_block_page',
    'weight' => 10,
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
    'access arguments' => array('administer site configuration'),
  );

  $items['admin/config/system/wechat_lite/service_accounts'] = array(
    'title' => 'Service Accounts',
    'description' => t('Configure WeChat Service Accounts.'),
    'type' => MENU_NORMAL_ITEM,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wechat_lite_service_accounts_settings'),
    'file' => 'wechat_lite.admin.inc',
    'access arguments' => array('administer site configuration'),
  );

  // Creates Add action link on parent page
  $items['admin/config/system/wechat_lite/service_accounts/add'] = array(
    'title' => 'Add Service Account',
    'type' => MENU_LOCAL_ACTION,
    'weight' => 1,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wechat_lite_service_account_form'),
    'file' => 'wechat_lite.admin.inc',
    'access arguments' => array('administer site configuration'),
  );

  $items['admin/config/system/wechat_lite/service_account/%'] = array(
    'title' => 'Edit service account',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wechat_lite_service_account_form', 5),
    'file' => 'wechat_lite.admin.inc',
    'access arguments' => array('administer site configuration'),
  );

  $items['admin/config/system/wechat_lite/create_menu/service_account/%'] = array(
    'title' => 'Service Account - Menu Creation',
    'page callback' => 'wechat_lite_admin_create_menu_service_account',
    'page arguments' => array(6),
    'file' => 'wechat_lite.admin.inc',
    'access arguments' => array('administer site configuration'),
  );

  $items['admin/config/system/wechat_lite/menu/%'] = array(
    'title' => ' - Menu Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wechat_lite_menu_form', 6),
    'file' => 'wechat_lite.admin.inc',
    'access arguments' => array('administer site configuration'),
  );

  $items['wechat_lite/service_account/server/%'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'wechat_lite_service_account_callback',
    'page arguments' => array(3),
    'file' => 'wechat_lite.service_account.inc',
    'access callback' => TRUE,
  );

  $items['wechat_lite/service_account/oauth/%'] = array(
    'title' => 'WeChat Lite Oauth',
    'description' => t('WeChat lite open authentication callback'),
    'page callback' => 'wechat_lite_service_account_oauth',
    'page arguments' => array(3),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'wechat_lite.service_account.inc',
  );

  $items['admin/config/system/wechat_lite/enterprise_accounts'] = array(
    'title' => 'Enterprise Accounts',
    'description' => 'Configure WeChat Enterprise Accounts.',
    'type' => MENU_NORMAL_ITEM,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wechat_lite_enterprise_accounts_settings'),
    'file' => 'wechat_lite.admin.inc',
    'access arguments' => array('administer site configuration'),
  );

  // Creates Add action link on parent page
  $items['admin/config/system/wechat_lite/enterprise_accounts/add'] = array(
    'title' => 'Add Enterprise Account',
    'type' => MENU_LOCAL_ACTION,
    'weight' => 1,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wechat_lite_enterprise_account_form'),
    'file' => 'wechat_lite.admin.inc',
    'access arguments' => array('administer site configuration'),
  );

  $items['admin/config/system/wechat_lite/enterprise_accounts/%'] = array(
    'title' => 'Edit Enterprise Account',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wechat_lite_enterprise_account_form', 5),
    'file' => 'wechat_lite.admin.inc',
    'access arguments' => array('administer site configuration'),
  );

  // enterprise account callback entry
  $items['wechat_lite/enterprise_account/%/server'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'enterprise_account_server',
    'page arguments' => array(2),
    'file' => 'wechat_lite.enterprise_account.inc',
    'access callback' => TRUE,
  );

  return $items;
}

/**
 * Implements hook_entity_info().
 *
 * @See https://www.drupal.org/node/1026420 How to create a new entity type
 */
function wechat_lite_entity_info() {
  $info = array();
  $info['wechat_lite_message'] = array(
    'label' => t('WeChat Lite Message'),
    'plural label' => t('WeChat Lite Messages'),
    'description' => t('WeChat Lite Message basic definition'),
    'entity class' => 'Entity',
    'controller class' => 'EntityAPIController',
    'base table' => 'wechat_lite_message',
    'fieldable' => true,
    'entity keys' => array(
      'id' => 'mid',
      'label' => 'MsgId',
      'bundle' => 'MsgType',
    ),
    'bundles' => array(
      'text' => array(
        'label' => 'text',
      ),
      'image' => array(
        'label' => 'image',
      ),
    ),
//  'uri callback' => 'entity_class_uri',
    'module' => 'wechat_lite',

  );

  return $info;
}

/**
 * Implements hook_cron().
 */
function wechat_lite_cron() {

  return;

  watchdog(
    WECHAT_LITE__HUMAN_NAME,
    "wechat_lite_cron start: %date",
    array('%date' => date_format(date_now('Asia/Shanghai'),'Y-m-d H:i:s'))
  );

  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'wechat_lite_message', '=')
    ->entityCondition('bundle', 'image')
    ->propertyCondition('status', 0, '=')
  ;

  $result = $query->execute();

  if (empty($result['wechat_lite_message'] )){
    return;
  }

  $messageIDs = array_keys($result['wechat_lite_message']);;

  $messages = entity_load('wechat_lite_message', $messageIDs);

  foreach ($messages as $msg){

    try {
      $file = media_parse_to_file($msg->PicUrl);
    }
    catch (Exception $e) {

      watchdog(
        WECHAT_LITE__HUMAN_NAME,
        __FUNCTION__." fetch error: %mid",
        array('%mid' => $msg->mid)
      );

      continue;
    }


    // data need be stored as private
    $uri = file_stream_wrapper_uri_normalize('private://' . $file->filename . '.jpg');
    $file = file_move($file, $uri);

    $msg->fid = $file->fid;

    $msg->save();
  }

  watchdog(
    WECHAT_LITE__HUMAN_NAME,
    "wechat_lite_cron end: %date",
    array('%date' => date_format(date_now('Asia/Shanghai'),'Y-m-d H:i:s'))
  );
}


/**
 * Implements hook_theme().
 */
function wechat_lite_theme($existing, $type, $theme, $path) {

  $themes = array(

  );

  return $themes;
}


/**
 * Fetching access_token through $appid, $appsecret.
 *
 * @param string $appid
 * @param string $appsecret
 *
 * @return string
 *   access token value.
 */
function wechat_lite_get_access_token_app($appid, $appsecret) {

  $get_token_url = format_string(WECHAT_LITE__URL__ACCESS_TOKEN_APP, array(
    '@APPID' => $appid,
    '@APPSECRET' => $appsecret,
  ));

  $json = wechat_lite_call_wechat_server($get_token_url);

  if (empty($json)) {
    return '';
  }

  return $json['access_token'];
}

/**
 * @return array json
 */
function wechat_lite_call_wechat_server($call_url) {

  $res = drupal_http_request($call_url);

  if (isset($res->data)) {

    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      __FUNCTION__ . ": token data = %data",
      array('%data' => $res->data),
      WATCHDOG_NOTICE
    );

    $json = json_decode($res->data, TRUE);

    if (!$json || !empty($json['errcode'])) {
      watchdog(
        WECHAT_LITE__HUMAN_NAME,
        __FUNCTION__ . " error(%error_code): %error",
        array(
          '%error_code' => (isset($json['errcode']))?$json['errcode']:'no_error_code',
        ),
        WATCHDOG_ERROR
      );

      return array();
    }

    return $json;
  }
  else {
    watchdog(
      WECHAT_LITE__HUMAN_NAME,
      __FUNCTION__ . " no respond: %call_url",
      array('%call_url' => $call_url),
      WATCHDOG_ERROR
    );

    return array();
  }
}
